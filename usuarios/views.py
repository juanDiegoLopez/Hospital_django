from django.shortcuts import render, redirect
from django.contrib.auth.views import LoginView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.forms import AuthenticationForm
from .forms import UsuarioForm, PerfilForm, LoginForm
from .models import Usuario
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin
from django.core.mail import EmailMessage # para usar el gmail
from django.contrib.sites.shortcuts import get_current_site # para el dominio
from django.template.loader import render_to_string  # toma un html y lo pone como un string
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode # codififcar y decodificar en base 64
from django.utils.encoding import force_bytes, force_text
from .token import token_activacion 
from django.views.generic import TemplateView
from django.contrib.auth.views import LoginView
from django.contrib import messages

from django.db.models import Count


class Login(LoginView):
    template_name = 'login.html'
    form_class = AuthenticationForm


class Nuevo(CreateView):
    template_name = 'nuevo.html'
    model = Usuario
    form_class = UsuarioForm
    success_url = reverse_lazy('usuarios:login')

    def form_valid(self, form):
        usuario_form = UsuarioForm(self.request.POST, self.request.FILES)
        
        if usuario_form.is_valid():
            
            user = form.save(commit=False)
            user.is_active = False
            user.save()

            current_site = get_current_site(self.request)
            dominio = get_current_site(self.request) #trae el dominio requiere importacion

            mensaje = render_to_string('confirmar_cuenta.html',
                {
                    'user': user,
                    'dominio': dominio,
                    'uid': urlsafe_base64_encode(force_bytes(user.id)),
                    'token': token_activacion.make_token(user)
                } # parametros que se mandan el html 
            )
            email = EmailMessage(
                'Activar cuenta', # asunto
                mensaje, 
                to=[user.email], # destinatario 
            ) # contenido del correo
            email.content_subtype = "html" ## para darle un formato o estilo al correo
            email.send()

        else:
            
            return self.render_to_response(self.get_context_data(form=form, extra_context = usuario_form))
        return super().form_valid(form)

             
class Perfil(SuccessMessageMixin, UpdateView):
    template_name = 'perfil.html'
    model = Usuario
    form_class = PerfilForm
    success_message = "El usuario %(first_name)s se actualizó con éxito"


    def get_success_url(self):
        pk = self.kwargs.get(self.pk_url_kwarg)
        url = reverse_lazy('usuarios:perfil', kwargs={'pk': pk})
        return url


class ActivarCuenta(TemplateView):
    

    def get(self, request, *args, ** kwargs):
        context = self.get_context_data(**kwargs)

        try:
            uid = urlsafe_base64_decode(kwargs['uidb64'])
            token = kwargs['token']
            user = Usuario.objects.get(pk = uid)
        except(TypeError, ValueError, Usuario.DoesNotExist):
            user = None
        
        if user is not None and token_activacion.check_token(user, token):
            user.is_active = True 
            user.save()
            
            messages.success(self.request, 'Cuenta activada, ingresar datos')
        else:
            messages.error(self.request, 'Token invalido, contacta al administrador ')

        return redirect('usuarios:login')

     

