from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Paciente, Municipio, Estado
from .forms import PacienteForm
from django.urls import reverse_lazy
from django.http import JsonResponse
from django.db.models import Count 
from django.db import models
from django.views.generic import TemplateView
from django.utils import  timezone
import datetime
from django_weasyprint import WeasyTemplateResponseMixin
from django.conf import settings
from django.views.generic.detail import DetailView
from django.views.generic import View

class Lista(ListView):

    paginate_by = 5 
    model = Paciente


class Nuevo(CreateView):
    model = Paciente
    form_class = PacienteForm
    
    success_url = reverse_lazy('pacientes:lista')

class Editar(UpdateView):
    model = Paciente
    form_class = PacienteForm
    extra_context = {'editar':True}

    success_url = reverse_lazy('pacientes:lista')

class Eliminar(DeleteView):
    model = Paciente
    success_url = reverse_lazy('pacientes:lista')


def buscar_municipio(request):
    id_estado = request.POST.get('id',None)
    if id_estado:
        municipios = Municipio.objects.filter(estado_id=id_estado)
        data = [{'id':mun.id,'nombre':mun.nombre} for mun in  municipios]
        return JsonResponse(data, safe=False)
    return JsonResponse({'error':'Parámetro inválido'}, safe=False)

class Grafica_sangre(TemplateView):
    template_name = 'pacientes/grafica_sangre.html'
    # todos los valores distintos de sangre contados por tipo
    pacientes_sangre = Paciente.objects.all().values('tipo_sangre').annotate(cuanto = Count('tipo_sangre'))
    # los tipos distintos de sangre que hay linea valida para mostrar solo los que se tienen
    #sangre_tipos = Paciente.objects.all().values('tipo_sangre').distinct()
   
    sangre_tipos = ['O+','O-', 'A-', 'A+']
    datos = []
     
    
    for sangre in sangre_tipos:
        cuantos = 0
        for tp in pacientes_sangre:
            #para cuando se cambia linea en sangre_tipos
            #if tp['tipo_sangre']==sangre['tipo_sangre']:
            if tp['tipo_sangre']== sangre:
                cuantos = tp['cuanto'] 
                break 
        datos.append({'name':tp['tipo_sangre'], 'data': [cuantos]})

    
    extra_context = {'datos': datos }
 

class Grafica_edades(TemplateView):

    template_name = 'pacientes/grafica_edad.html'
    # todos las edades con conteo por edad
    pacientes_edades = Paciente.objects.all().dates('fecha_nac','day').annotate(cuanto = Count('fecha_nac'))
    #pacientes_edades = Paciente.objects.raw( Select  * from   Paciente)
    # las distintas edades que tienen los pacientes
    edades = Paciente.objects.dates('fecha_nac','year')
   
    datos = []
    now = datetime.datetime.now()
    for edad in edades:
        cuantos = 0
        for ed in pacientes_edades:
            if ed.year== edad.year:
                cuantos = cuantos + 1
        datos.append({'name': (now.year - edad.year) , 'data': [cuantos]})

    
    extra_context = {'datos': datos}
 
class Vista_lista_pacientes_PDF(ListView):
    model = Paciente
    template_name = 'pacientes/lista_pacientes_PDF.html'
    #extra_context = {"suma": suma}

class Lista_pacientes_pdf(WeasyTemplateResponseMixin, Vista_lista_pacientes_PDF ):
    
    pdf_stylesheets =  [
        settings.STATICFILES_DIRS[0] + 'dist/css/adminlte.min.css',
        settings.STATICFILES_DIRS[0] + 'dist/css/estilos.css',
    ]
    pdf_attachment = False
    pdf_filename = 'pacientes_lista.pdf'


class Vista_paciente_pdf(CreateView):
    model = Paciente
    template_name = 'pacientes/paciente_pdf.html'
    form_class = PacienteForm
    extra_context = {'paciente_pdf':True}

class Paciente_pdf(WeasyTemplateResponseMixin, Vista_paciente_pdf):
    pdf_stylesheets =  [
        settings.STATICFILES_DIRS[0] + 'dist/css/adminlte.min.css',
        settings.STATICFILES_DIRS[0] + 'dist/css/estilos.css',
    ]
    pdf_attachment = False
    pdf_filename = 'paientes.pdf'

