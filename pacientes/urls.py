from django.urls import path
from .views import Lista, Nuevo, Eliminar, Editar, buscar_municipio, Grafica_sangre, Grafica_edades, Lista_pacientes_pdf, Paciente_pdf
app_name = 'pacientes'

from .views import Vista_paciente_pdf
urlpatterns = [
    path('lista/', Lista.as_view(), name='lista'),
    path('nuevo/', Nuevo.as_view(), name='nuevo'),
    path('editar/<int:pk>', Editar.as_view(), name='editar'),
    path('eliminar/<int:pk>', Eliminar.as_view(), name='eliminar'),
    path('busca-municipio/', buscar_municipio, name='buscar_municipio'),
    path('grafica_edad/', Grafica_edades.as_view(), name='grafica_edades'),
    path('grafica_sangre/', Grafica_sangre.as_view(), name='grafica_sangre'),
    path('pdf/', Lista_pacientes_pdf.as_view(), name='pdf_pacientes'),
    path('paciente_pdf/<int:pk>', Vista_paciente_pdf.as_view(), name='paciente_pdf')

]
